If you want to contribute to the development you can either:
1. Open an issue with `Feature` tag using our [Issue tracker](https://gitlab.com/MartinBeseda/sa-oo-vqe-qiskit/-/issues) describing a new functionality suggestion.
2. Fork the repository, implement the functionality you like and submit a pull request to this repository.

For your contribution to be recognized, it has to be:
1. Fully documented via Sphinx docstrings
2. Completely covered by automated tests using `pytest`
3. Equipped with an example in Jupyter notebook illustrating the new functionality
